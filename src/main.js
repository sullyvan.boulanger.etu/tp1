let data = [
	{
		name: 'Regina',
		base: 'tomate',
		price_small: 6.5,
		price_large: 9.95,
		image: 'https://images.unsplash.com/photo-1532246420286-127bcd803104?fit=crop&w=500&h=300'
	},
	{
		name: 'Napolitaine',
		base: 'tomate',
		price_small: 6.5,
		price_large: 8.95,
		image: 'https://images.unsplash.com/photo-1562707666-0ef112b353e0?&fit=crop&w=500&h=300'
	},
	{
		name: 'Spicy',
		base: 'crème',
		price_small: 5.5,
		price_large: 8,
		image: 'https://images.unsplash.com/photo-1458642849426-cfb724f15ef7?fit=crop&w=500&h=300',
	}
];

let html="";
let a="";
let section="";
let ul="";

/*data.sort(function (a, b) {
    return a.name.localeCompare(b.name);
}); // Tri sur le nom*/

/*data.sort(function (a, b) {
    return a.price_small -b.price_small;
});//Tri sur le prix des petits formats*/ 

/*data.sort(function (a, b) {
    if(a.price_small -b.price_small!=0){
        return a.price_small -b.price_small;
    }else{
        return a.price_large -b.price_large;
    }
});//Tri sur le prix des petits formats et si égalité, Tri sur le prix des grands formats*/

data
.filter(pizza => pizza.name.match(/i/g).length ==2)
.forEach(element => {
    ul=`<ul><li>Prix petit format : ${element.price_small} €</li><li>Prix grand format : ${element.price_large} €</li></ul>`;
    section=`<section>w<h4>${element.name}</h4>${ul}</section>`;
    a=`<a href="${element.image}"/><img src="${element.image}"/>${section}</a>`
    html+=`<article class="pizzaThumbnail">${a}</article>`;
});

document.querySelector('.pageContent').innerHTML = html;